package openpgpjs.test.features;

import openpgpjs.Openpgp;
import openpgpjs.test.matchers.*;

import org.hamcrest.Matchers.*;
import massive.munit.async.AsyncFactory;

class GenerateKeyTest extends TestScenario{
    @steps
    var steps:TestSteps;

    @scenario
    @Test
    public function generate_key_promise(){
        given().the_options_to_generate_a_key();
        when().generating_the_key_pair();
        then().a_non_null_promise_should_be_returned();
    }

    @AsyncTest
    @scenario
    public function generate_key_resolve(factory:AsyncFactory){
      given().a_key_generation_was_started();
      when().the_promise_is_resolved(factory.createHandler(this, function () {
          then().a_valid_key_should_have_been_received();
      }));
    }
}

private class TestSteps extends hxgiven.Steps<TestSteps>{
    var options:GenerateKeyOptions;
    var keyPromise:js.Promise<GeneratedKey>;
    var receivedKey:GeneratedKey;

    public function new(){
        super();
    }

    @step
    public function the_options_to_generate_a_key(){
        given().these_generate_key_options({
            userIds: [{ name:'Jon Smith', email:'jon@example.com' }], // multiple user IDs
            curve: "ed25519",
            passphrase: 'super long and hard to guess secret'         // protects the private key
        });
    }

    @step
    public function these_generate_key_options(options: GenerateKeyOptions){
        this.options = options;
    }

    @step
    public function a_key_generation_was_started(){
        given().the_options_to_generate_a_key();
        when().generating_the_key_pair();
    }


    @step
    public function generating_the_key_pair(){
        keyPromise = Openpgp.generateKey(options);
    }

    @step
    public function the_promise_is_resolved(handler: Void->Void){
        assertThat(keyPromise, is(notNullValue()));

        if( handler != null ) {
            keyPromise.then(function ( key: GeneratedKey ) : Void {
                receivedKey = key;

                handler();
            });
        }
    }


    @step
    public function a_non_null_promise_should_be_returned(){
        assertThat(keyPromise, is(notNullValue()));
    }

    @step
    public function a_valid_key_should_have_been_received(){
        assertThat(receivedKey, is(notNullValue()));

        assertThat(receivedKey.key                  , is(notNullValue()));
        assertThat(receivedKey.privateKeyArmored    , is(new NotEmptyString()));
        assertThat(receivedKey.publicKeyArmored     , is(new NotEmptyString()));
        assertThat(receivedKey.revocationCertificate, is(new NotEmptyString()));
    }
}
