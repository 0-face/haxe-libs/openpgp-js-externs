package openpgpjs.test;

class TestScenario implements hxgiven.Scenario{

  @Before
  public function reset(): Void{
    var field = Reflect.field(this, "resetSteps");
  	if (field != null && Reflect.isFunction(field)){
      Reflect.callMethod(this, field, []);
    }
  }
}
