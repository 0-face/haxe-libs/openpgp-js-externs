package openpgpjs.test.matchers;

import org.hamcrest.CustomTypeSafeMatcher;
import org.hamcrest.Description;

class NotEmptyString extends CustomTypeSafeMatcher<String> {

    public function new(?description: String) {
        super(if(description == null) "a non empty string" else description);
    }

    public override function isExpectedType(value:Dynamic):Bool{
        return Std.is(value, String);
    }

    public override function matchesSafely( s: String ) : Bool {
        return s != null && s.length > 0;
    }

    public override function describeMismatchSafely(s: String, mismatchDescription: Description) {
        mismatchDescription.appendText("was empty");
    }
}
