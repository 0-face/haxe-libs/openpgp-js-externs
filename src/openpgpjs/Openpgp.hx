package openpgpjs;

typedef GenerateKeyOptions = {
    /** array of user IDs e.g. [{ name:'Phil Zimmermann', email:'phil@openpgp.org' }] */
    userIds: Array<Dynamic>,
    ?passphrase: String,
    ?numBits: Int,
    ?keyExpirationTime:Int,
    ?curve:String,
    ?date:Date,

    /** options for each subkey, default to main key options.
        e.g. [{sign: true, passphrase: '123'}]
        sign parameter defaults to false, and indicates whether the subkey
        should sign rather than encrypt
    */
    ?subkeys:Array<Dynamic>
};

typedef GeneratedKey = {
    key                  : Dynamic,
    privateKeyArmored    : String,
    publicKeyArmored     : String,
    revocationCertificate: String
};

@:jsRequire("openpgp")
extern class Openpgp{
    static function generateKey(options: GenerateKeyOptions) : js.Promise<GeneratedKey>;
}
